<?php
namespace App;

class Response
{


	protected $body,
			  $statusCode = 200,
			  $headers = [];


	public function withBody( $body )
	{

		$this->body = $body;
		return $this;

	}

	public function getBody(){

		return $this->body;
	}

	public function withStatusCode( $statusCode)
	{

		$this->statusCode = $statusCode;
		return $this;
	}

	public function getStatusCode(){

		return $this->statusCode;
	}

	public function withJson( $body )
	{

		$this->withHeader('Content-Type', 'application/json');
		$this->body = json_encode( $body );
		return $this;
	}

		public function withHeader( $name, $value)
	{

		$this->headers[] = [$name, $value];

	}

	public function getHeaders(){

		echo 'getHEader';
		return $this->headers;
	}


}