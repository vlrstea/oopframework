<?php
namespace App;
use App\Exceptions\MethodNotFoundException;
use App\Exceptions\RouteNotFoundException;

class Router
{

	protected $routes = [],
			  $methods = [],
			  $path;

	public function setPath( $path = '/')
	{


		$this->path = $path;
	}

	public function addRoute( $uri, $handler, array $methods = ['GET'] )
	{

		$this->routes[ $uri ] = $handler;
		$this->methods[ $uri ] = $methods;
	}

	public function response()
	{	

		if( !isset( $this->routes[ $this->path ]) ) 
		{

			throw new RouteNotFoundException('No route found for ' . $this->path );
		}


		if( !in_array( $_SERVER['REQUEST_METHOD'], $this->methods[ $this->path ]))
		{

			throw new MethodNotFoundException;
		}


		return $this->routes[ $this->path ];

	}
}