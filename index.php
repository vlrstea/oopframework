<?php


//loading the autoloader
require_once 'vendor/autoload.php';



//initialize the App and the container
$app = new App\App( new App\Container(

	[ 
		'router' => function(){


			return new App\Router;
		},
		
		'response' => function()
		{

			return new App\Response;

		}


	]

));



$container = $app->getContainer();

$container['errorHandler'] = function( ){

	return function( $response ){

		return $response->withBody('Page Not Found')->withStatusCode(404);
	};

};

$container['config'] = function(){


	return [

		'db_drive' => 'mysql',
		'db_host' => '127.0.0.1',
		'db_name' => 'mini',
		'db_user' => 'root',
		'db_password' => 'parola',

	];

};


$container['db'] = function($c){


	return new \PDO(
		$c->config['db_drive'] . ':host=' . $c->config['db_host'] . ';dbname=' . $c->config['db_name'], 
		$c->config['db_user'], 
		$c->config['db_password']);

};


$app->get('/', [new App\Controllers\HomeController($container->db), 'index']);


$app->run();



