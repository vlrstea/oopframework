<?php
namespace App;
use ArrayAccess;

class Container implements ArrayAccess
{
	
	protected $items = [],
			  $cache = [];




	public function __construct( array $items = [] )
	{

		foreach ($items as $key => $item) {

			$this->offsetSet( $key, $item );

		}

	}


	public function offsetSet( $offset, $closure)
	{

		$this->items[ $offset ] = $closure;
	}


	public function offsetGet( $offset )
	{

		if( !$this->has( $offset ))
		{

			return null;
		}


		//if a cache exists, return the cache instead
		if( isset( $this->cache[ $offset ] ) )
		{

			return $this->cache[ $offset ];
		}


		$item = $this->items[ $offset ]($this);

		//setting the cache
		$this->cache[ $offset ] = $item;


		return $item;
	}


	public function offsetUnset( $offset ) 
	{
		if( $this->has( $offset ) )
		{

			unset( $this->items[ $offset ]);
		}
		
	}


	public function offsetExists( $offset )
	{
		return isset( $this->items[ $offset ] );
		
	}


	public function has( $offset )
	{

		return $this->offsetExists( $offset );
	}
	

	public function __get( $property )
	{

		return $this->offsetGet( $property );
	}

}